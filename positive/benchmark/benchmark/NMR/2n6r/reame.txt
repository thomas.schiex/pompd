This directory contains the pdb and resfiles for the Σ-MSD (minimum
average energy multistate design) of one protein, described by four
selected NMR states.

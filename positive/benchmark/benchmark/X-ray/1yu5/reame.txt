This directory contains the pdb and resfiles for the Σ-MSD (minimum
average energy multistate design) of one protein, described by four
states obtained by selecting 4 states among a set of 100 backrubbed
variants of the original pdb file.

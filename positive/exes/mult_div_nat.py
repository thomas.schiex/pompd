#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 11:37:53 2018

@author: mruffini

"""

from collections import OrderedDict
import sys

import argparse
import time
import utils

AAs = "ARNDCQEGHILKMFPSTWYV"
toulbar2 = "toulbar2 "

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", required=True,
                    help="The input file to process (.cfn.gz)", metavar="FILE")
parser.add_argument("-o", "--output", default=None,
                    help="The name of the output file where the solutions are written")
parser.add_argument("-n", "--nat", required=True,
                    help="Filename of native sequence file")
parser.add_argument("--natdiv", default=1, type=int,
                    help="Minimum diversity from native sequence")
parser.add_argument("--divmin", default=1, type=int,
                    help="Hard constraint - minimum dissimilarity distance from each previous solution\n (divmin "
                         "differences is accepted ; =0 if no constraint)")
parser.add_argument("--nsols", default=1, type=int,
                    help="Number of diverse good solutions to compute")
parser.add_argument("--lim", default=None, type=int,
                    help="Time limit for toulbar2 (for each solution) in seconds")

# CPD
parser.add_argument("--cpd", action="store_true", default=False,
                    help="Computational Protein Design - addition of sequence variables + possiblity of using a "
                         "similarity matrix")
parser.add_argument("--msim", default=None,
                    help="Similarity matrix - If None: Hamming distance")

args = parser.parse_args()

cfn_filename = args.input
cfn = utils.read_cfn_gzip(cfn_filename)
if cfn_filename[-7:] == '.cfn.gz':
    name = cfn_filename[:-7]
else:
    name = cfn_filename

sols_filename = args.output if args.output else \
    name + '_divmin' + str(args.divmin) + "_k" + str(args.nsols) + ".sol"

cfn_tmp = name + '_divmin' + str(args.divmin) + '.tmp.cfn'
n_vars = len(cfn['variables'])

with open(args.nat, 'r') as natfile:
    for line in natfile:
        nat = list(line)[:-1]
    natfile.close()

# Sequence variables (cpd)
# vars_list = list of variables on which the diversity contraint applies
# if cpd: sequence variables
if args.cpd:
    utils.add_seq_vars(cfn)
    vars_list = list(cfn['variables'].keys())[n_vars:]
    val_list = AAs
else:
    vars_list = list(cfn['variables'].keys())
    # val_list : only if there is a similarity matrix
    # All variables must have the same domain ; all values in the order corresponding to the one in the similarity matrix
    val_list = list(cfn['variables'].values())[0]

############################

# Functions to create a constraint from a previous solution

def counting_bin_dissim(vars_list, sol, sol_name, divmin, val_list=None, msim=None):
    n_vars = len(vars_list)
    q = 'q_' + sol_name + '_'
    cfn['variables'][q + '-1'] = 1
    c = 'c_' + sol_name + '_'
    max_dissim = 1
    if msim is not None:
        max_dissim = max([max(l) for l in msim])
    for i in range(n_vars):
        var = vars_list[i]
        domain = utils.get_domain(var, cfn)
        qi = q + str(i)
        cfn['variables'][qi] = divmin + 1
        qip = q + str(i - 1)
        ci = c + str(i)
        cfn['variables'][ci] = (max_dissim + 1) * (divmin + 1)
        fca = f'f_{ci}_{var}'
        cfn['functions'][fca] = OrderedDict()
        cfn['functions'][fca]['scope'] = [ci, var]
        cfn['functions'][fca]['defaultcost'] = float(cfn['problem']['mustbe'][1:])
        cfn['functions'][fca]['costs'] = []
        fcqi = f'f_{ci}_{qi}'
        cfn['functions'][fcqi] = OrderedDict()
        cfn['functions'][fcqi]['scope'] = [ci, qi]
        cfn['functions'][fcqi]['defaultcost'] = float(cfn['problem']['mustbe'][1:])
        cfn['functions'][fcqi]['costs'] = []
        fcqip = f'f_{ci}_{qip}'
        cfn['functions'][fcqip] = OrderedDict()
        cfn['functions'][fcqip]['scope'] = [ci, qip]
        cfn['functions'][fcqip]['defaultcost'] = float(cfn['problem']['mustbe'][1:])
        cfn['functions'][fcqip]['costs'] = []
        delta_seen = []
        for val_index in range(len(domain)):
            if args.cpd:
                val = domain[val_index]
                delta = utils.dissim(val, sol[i], val_list, msim)
                # print(f'val: {val}, sol {sol[i]}, delta {delta}')
            else:
                delta = utils.dissim(val_index, sol[i], val_list, msim)
            qip_add = False
            if delta not in delta_seen:
                qip_add = True
                delta_seen.append(delta)
            for qip_val in range(len(utils.get_domain(qip, cfn))):
                qi_val = min(qip_val + delta, divmin)
                ci_val = delta * (divmin + 1) + qip_val
                cfn['functions'][fca]['costs'] += [ci_val, val_index, 0]
                if qip_add:
                    cfn['functions'][fcqi]['costs'] += [ci_val, qi_val, 0]
                    cfn['functions'][fcqip]['costs'] += [ci_val, qip_val, 0]
    # Adding unary cost on Qn
    cfn['functions']['unary_' + qi] = OrderedDict()
    cfn['functions']['unary_' + qi]['scope'] = [qi]
    cfn['functions']['unary_' + qi]['costs'] = []
    for j in range(divmin):
        cfn['functions']['unary_' + qi]['costs'].append(float(cfn['problem']['mustbe'][1:]))
    cfn['functions']['unary_' + qi]['costs'].append(0)


## Compute k solutions



if args.msim != None:
    msim = utils.read_sim_mat(args.msim)
else:
    msim = None
    val_list = None

counting_bin_dissim(vars_list, nat, "native", args.natdiv, val_list, msim)
utils.write_cfn(cfn, args.input[:-7]+"_natdiv.cfn")

utils.write_cfn(cfn, cfn_tmp)
sols_file = open(sols_filename, 'w')
tb2log = "tmp.tb2"
time_limit = ""
if args.lim is not None:
    time_limit = " -timer=" + str(args.lim) + " "
tb2_cmd = toulbar2 + cfn_tmp + time_limit + ' -m -hbfs: -s -w="tmp.sol" | tee > ' + tb2log
all_sols = []

for k in range(args.nsols):
    utils.execute("Looking for solution " + str(k + 1) + " with toulbar2", tb2_cmd)
    (opt, sol) = utils.get_optimum(tb2log)
    if sol[:n_vars] in all_sols or sol is None:
        break
    sols = [sol[:n_vars]]
    all_sols.append(sol[:n_vars])
    sol_name = "sol" + str(k + 1)
    # print solution and add it to the solution file
    sols_file.write("Solution " + str(k + 1) + '\n')
    sols_file.write(' '.join([str(val) for val in sols[0]]))
    print(' '.join([str(val) for val in sols[0]]))
    sols_file.write('\n')
    if args.cpd:
        utils.sols_to_cpd_sols(sols, cfn)
        sols_file.write(' '.join([str(val) for val in sols[0]]))
        print(' '.join([str(val) for val in sols[0]]))
        sols_file.write('\n')
    sols_file.write(str(opt) + '\n')
    if (k < args.nsols - 1):
        counting_bin_dissim(vars_list, sols[0], sol_name, args.divmin, val_list, msim)
        utils.write_cfn(cfn, cfn_tmp)

sols_file.close()

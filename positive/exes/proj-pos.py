#!/usr/bin/env python

from __future__ import print_function
import math
import argparse
import os, itertools,json

PARPATH = "./pars"

def dumpconf(conf, filen, beg, count):
    with open(filen,'w') as f:
        for pos in range(beg,beg+count):
            f.write(str(conf[pos])+' ')

def dumplib(confs, filen, beg, count):
    with open(filen,'w') as f:
        for conf in confs:
            # Assuming AA variables are always first in the cfns (before sequence and dist constraint variables)
            for pos in range(beg,beg+count):
                f.write(str(conf[pos])+' ')
            f.write('\n')

def dumpenum(enum, filen, beg, count):
    with open(filen,'w') as f:
        for s in enum[:-1]: # not the total number
            f.write(s[beg:beg+count]+'\n')


##########################################################    
# main

parser = argparse.ArgumentParser(description='Projects positive multistate conformations and sequences on original states.')
parser.add_argument('matrix',  
                    help="The multistate .cfn.gz matrix (default 'positive.cfn.gz')")

parser.add_argument('--conf', action="store_true",
    default = False,
    help = 'Project GMEC conformation' )

parser.add_argument('--libconf', action="store_true",
    default = False,
    help = 'Project library conformations' )

parser.add_argument('--enum', action="store_true",
    default = False,
    help = 'Project enumerated sequences' )

args = parser.parse_args()
matrix = args.matrix if args.matrix else 'positive.cfn.gz'
wlst_file = os.path.splitext(os.path.splitext(matrix)[0])[0]+'.wlst'
conf_file = os.path.splitext(wlst_file)[0]+'.gmec'
lib_file = os.path.splitext(wlst_file)[0]+'.lib'
enum_file = os.path.splitext(wlst_file)[0]+'.enum'

# read the dictionary of original cfns + indices
with open(wlst_file,'r') as wf:
    cfns = json.load(wf)

if (args.conf):
    with open(conf_file,'r') as cf:
        gmec = [int(x) for x in cf.readline().split()]
    for cfn in cfns.keys():
        start,length = cfns[cfn]
        dumpconf(gmec,
                 os.path.splitext(os.path.splitext(cfn)[0])[0]+'.gmec+',
                 start, length)

if (args.libconf):
    gmecs = []
    with open(lib_file, 'r') as lf:
        for line in lf.readlines():
            gmecs.append([int(x) for x in line.split()])
    for cfn in cfns.keys():
        start,length = cfns[cfn]
        dumplib(gmecs,
                 os.path.splitext(os.path.splitext(cfn)[0])[0]+'.lib+',
                 start, length)


if (args.enum):
    with open(enum_file,'r') as ef:
        enum = [x.split()[2] for x in ef.readlines()]
    for cfn in cfns.keys():
        start,length = cfns[cfn]
        dumpenum(enum,
                 os.path.splitext(os.path.splitext(cfn)[0])[0]+'.enum+',
                 start, length)


            

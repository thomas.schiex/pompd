#!/usr/bin/env python3

# This needs to be executed in the directory that defines a "protein state" in iCFN
# it should produce a CFN.cfn.gz file that describe the energy in a format compatible
# with the MSD scripts.

# if an argument is given, it is a solution file and its cost will be computed

import struct
from collections import OrderedDict
from decimal import Decimal
from utils import write_cfn
import sys

precision = 6
energyformat = "{:."+str(precision)+"f}"

# for now, let's convert protonated variants to specific codes (also introduced in tb2 cpd now)
code2AA = "ARNceDCQdfEGHgijILMFPSTWYVK"
AA3to1 = {'ALA': 'A', 'ARG': 'R', 'ASN': 'N', 'ASP': 'D', 'CYS': 'C', 'GLU': 'E', 'GLN': 'Q', 'GLY': 'G',
          'HIS': 'H', 'ILE': 'I', 'LEU': 'L', 'LYS': 'K', 'MET': 'M', 'PHE': 'F', 'PRO': 'P', 'SER': 'S', 'THR': 'T',
          'TRP': 'W', 'TYR': 'Y', 'VAL': 'V', 'AS1': 'c', 'AS2': 'e', 'GL1': 'd', 'GL2': 'f', 'HSD': 'g', 'HID' : 'g',
          'HSE': 'i', 'HIE' : 'i', 'HSP': 'j', 'HIP': 'j'}

domainsizes = []
chains = []
positions = []
offsets = []
natives3 = []
native = ""
offset = 0
E0 = 0.0

with open("rot_positions", 'r') as f:
    for line in f:
        (chain, pos, AA3, rotnb) = line.split()
        chains.append(chain)
        positions.append(pos)
        offsets.append(offset)
        natives3.append(AA3)
        native = native+AA3to1[AA3]
        domainsizes.append(int(rotnb))
        offset += int(rotnb)


# read the amino acid that match each rotamer for all positions
rot2AA = []
with open("amino.txt", 'r') as f:
    for line in f:
        AAcode = int(line)
        rot2AA.append(code2AA[AAcode])

# read E template
E0 = 0.0
with open("const.bind.e1.dat", 'rb') as f:
    E0 = struct.unpack('d', f.read(8))[0]

# read unaries and binaries
nbvar = len(positions)
nbbin = (nbvar*(nbvar-1))//2
U = []
for i in range(nbvar):
    U.append([])
B = []    
for i in range(nbbin):
    B.append([])
    
with open("energies.bind.e1.dat", 'rb') as f:
    for v in range(nbvar):
        for rot in range(domainsizes[v]):
            U[v].append(struct.unpack('d', f.read(8))[0])

    vidxs = {}
    vidx = 0
    for v1 in range(nbvar):
        for v2 in range(v1+1, nbvar):
            vidxs[v1,v2] = vidx
            vidx += 1
            
    for v1 in range(nbvar):
        for r1 in range(domainsizes[v1]):
            for v2 in range(v1+1, nbvar):
                  for r2 in range(domainsizes[v2]):
                    B[vidxs[v1,v2]].append(struct.unpack('d', f.read(8))[0])
energy = E0
if len(sys.argv) > 1:
    sol = []
    with open(sys.argv[1]) as fsol:
        sol = [int(x) for x in fsol.readline().split()]
    for i in range(len(positions)):
        oneb =  U[i][sol[i]]
        energy += oneb
        for j in range(i+1,len(positions)):
            twob = B[vidxs[i,j]][sol[i]*domainsizes[j]+sol[j]]
            energy += twob
    print(energy)


# Now create the CFN structures
mutables = 0
ub = 0.0
varDict = OrderedDict()
varnames = []
nativeseq = ""
mutables = 0
# write variables and rotamers
for res, resid in enumerate(positions):
    natAA = native[res]
    values = []
    mutable = False
    for i in range(domainsizes[res]):
        rotAA = rot2AA[offsets[res]+i]
        if (rotAA != natAA):
            mutable = True
        values.append(rotAA+str(i))
    mutables += mutable
    varname = natAA+str(resid)+chains[res]  # add chain name to be safe
    varnames.append(varname)
    varDict[varname] = values

ub = E0
# write cost function
funcsDict = OrderedDict()
funcDict0 = OrderedDict()
funcDict0['scope'] = []
funcDict0['costs'] = [Decimal(energyformat.format(E0))]
funcsDict['E_template'] = funcDict0

for res1, resid in enumerate(positions):
    max_energy = -float("inf")
    isEmpty = True
    etable = []
    for energy in U[res1]:
        etable.append(Decimal(energyformat.format(energy)))
        max_energy = max(energy, max_energy)
        if energy != 0.0:
            isEmpty = False

    ub += max_energy
    if (not isEmpty):
        funcDict = OrderedDict()
        funcDict['scope'] = [varnames[res1]]
        funcDict['costs'] = etable
        funcsDict['E'+str(res1)] = funcDict
        print('E'+str(res1),funcDict['scope'])

Bidx = 0
for res1, resid1 in enumerate(positions):
    for res2 in range(res1+1, len(positions)):
        resid2 = positions[res2]
        max_energy = float("-inf")
        energyl = []
        binary = B[Bidx]
        isEmpty = True
        for energy in binary:
            max_energy = max(energy, max_energy)
            energyl.append(Decimal(energyformat.format(energy)))
            if energy != 0.0:
                isEmpty = False
        if (not isEmpty):
            funcDict = OrderedDict()
            funcDict['scope'] = [varnames[res1], varnames[res2]]
            funcDict['costs'] = energyl
            funcsDict['E'+str(res1)+"_"+str(res2)] = funcDict
            ub += max_energy
            print('E'+str(res1)+"_"+str(res2),funcDict['scope'])
        Bidx += 1

energy = E0
if len(sys.argv) > 1:
    sol = []
    with open(sys.argv[1]) as fsol:
        sol = [int(x) for x in fsol.readline().split()]
    for i in range(len(positions)):
        oneb =  funcsDict["E"+str(i)]["costs"][sol[i]]
        energy += float(oneb)
        for j in range(i+1,len(positions)):
            name = "E"+str(i)+"_"+str(j)
            if name in funcsDict:
                twob = funcsDict[name]["costs"][sol[i]*domainsizes[j]+sol[j]]
                energy += float(twob)
    print(energy)


header = OrderedDict()
header['name'] = 'iCFN_source'
header['mustbe'] = '<'+energyformat.format(ub+pow(10,-precision))

cfn = OrderedDict()
cfn['problem'] = header
cfn['variables'] = varDict
cfn['functions'] = funcsDict

write_cfn(cfn, "CFN.cfn.gz", native + " " + str(mutables) + " 0.0")

#!/usr/bin/env python3

# This needs to be executed in the directory that contains the directories with the
# "protein substates" in iCFN. it should produce a CFN.cfn.gz file that describe the
# "min"energy in CFN format. This contains extra variable that are not purely MSD
# compatible (TODO)
# Arguments: all substate directories

import struct
import sys
import os
from functools import reduce
from collections import OrderedDict
from decimal import Decimal
from utils import write_cfn

top = 100000
precision = 6
energyformat = "{:."+str(precision)+"f}"

# for now, let's convert protonated variants to specific codes (also introduced in tb2 cpd now)
code2AA = "ARNceDQdfEGHgijILMFPSTWYVK"
AA3to1 = {'ALA': 'A', 'ARG': 'R', 'ASN': 'N', 'ASP': 'D', 'CYS': 'C', 'GLU': 'E', 'GLN': 'Q', 'GLY': 'G',
          'HIS': 'H', 'ILE': 'I', 'LEU': 'L', 'LYS': 'K', 'MET': 'M', 'PHE': 'F', 'PRO': 'P', 'SER': 'S', 'THR': 'T',
          'TRP': 'W', 'TYR': 'Y', 'VAL': 'V', 'AS1': 'c', 'AS2': 'e', 'GL1': 'd', 'GL2': 'f', 'HSD': 'g', 'HID' : 'g',
          'HSE': 'i', 'HIE' : 'i', 'HSP': 'j', 'HIP': 'j'}

myArgv = sys.argv[1:]
allDomainsizes = []  # domainsize = number of rotamers overall at a given optimized residue
allChains = []      # chain letter for each optimized residue
allPositions = []   # position on the chain of each optimized residue
maxDomainsizes = []  # maximum number of rotamers over all substates for each position
allOffsets = []     # offset to the start of rotamers for an AA type
allNative = []      # native sequence
allE0 = []          # template energy
allU = []           # unary energy terms
allB = []           # binary energy terms

for bbidx, bb in enumerate(myArgv):
    print("Processing substate", bb)
    domainsizes = []
    chains = []
    positions = []
    offsets = []
    native = ""
    offset = 0
    E0 = 0.0

    with open(os.path.join(bb, "rot_positions"), 'r') as f:
        for line in f:
            (chain, pos, AA3, rotnb) = line.split()
            chains.append(chain)
            positions.append(pos)
            offsets.append(offset)
            native = native+AA3to1[AA3]
            domainsizes.append(int(rotnb))
            offset += int(rotnb)
    print("Read rot_positions")

    # read the amino acid that match each rotamer for all positions
    rot2AA = []
    with open(os.path.join(bb, "amino.txt"), 'r') as f:
        for line in f:
            AAcode = int(line)
            rot2AA.append(code2AA[AAcode])
    print("Read amino.txt")

    # read E template
    E0 = 0.0
    with open(os.path.join(bb, "const.bind.e1.dat"), 'rb') as f:
        E0 = struct.unpack('d', f.read(8))[0]
    print("Read const.bind.e1.dat")

    # read unaries and binaries
    nbvar = len(positions)
    nbbin = (nbvar*(nbvar-1))//2
    U = []
    for i in range(nbvar):
        U.append([])
    B = []    
    for i in range(nbbin):
        B.append([])
    
    with open(os.path.join(bb, "energies.bind.e1.dat"), 'rb') as f:
        for v in range(nbvar):
            for rot in range(domainsizes[v]):
                U[v].append(struct.unpack('d', f.read(8))[0])
                
        vidxs = {}
        vidx = 0
        for v1 in range(nbvar):
            for v2 in range(v1+1, nbvar):
                vidxs[v1,v2] = vidx
                vidx += 1

        for v1 in range(nbvar):
            for r1 in range(domainsizes[v1]):
                for v2 in range(v1+1, nbvar):
                    for r2 in range(domainsizes[v2]):
                        B[vidxs[v1,v2]].append(struct.unpack('d', f.read(8))[0])

    print("Read energies.bind.e1.dat")
    allDomainsizes.append(domainsizes)
    allChains.append(chains)
    allPositions.append(positions)
    allOffsets.append(offsets)
    allNative.append(native)
    allE0.append(E0)
    allU.append(U)
    allB.append(B)

# Now create the CFN structures. We use ternary cost functions to encode all the binary cost functions
# that exists between 2 residues in all sub-states. The third variable is the substate index and selects
# the corresponding energy. Because the domains for a given positions may be different between 2 substates
# (different number of rotamers for the same AA), we must use a virtual domain for each position that contains
# the maximum domain size. This is not tb2 cpd compatible in the sense that a value name will have no meaning.
# This would be feasible but requires to compute the maximum rotamer number per AA type. Not now.
maxDomainsizes = list(
    reduce(lambda la, lb: map(max, la, lb), allDomainsizes))

ub = 0.0
varDict = OrderedDict()
varnames = []
nativeseq = ""
varDict["SS"] = len(myArgv)  # the substate variable

# write variables and values.
for res, resid in enumerate(allPositions[0]):
    natAA = native[res]
    values = []
    for i in range(maxDomainsizes[res]):
        values.append('Z'+str(i))  # don't care which AA this is. See above.
    varname = natAA+str(resid)+chains[res]  # add chain name to be safe
    varnames.append(varname)
    varDict[varname] = values

ub = [x for x in allE0]
# write cost functions
funcsDict = OrderedDict()
funcDict0 = OrderedDict()
funcDict0['scope'] = ['SS']
funcDict0['costs'] = list(
    map(lambda x: Decimal(energyformat.format(x)), allE0))
funcsDict['E_template'] = funcDict0

for res1, resid in enumerate(allPositions[0]):
    etable = []
    for bbidx, bb in enumerate(myArgv):
        max_energy = -float("inf")
        for energy in allU[bbidx][res1]:
            etable.append(Decimal(energyformat.format(energy)))
            max_energy = max(energy, max_energy)
        dummynb = maxDomainsizes[res1]-len(allU[bbidx][res1])
        for i in range(dummynb):
            etable.append(Decimal(energyformat.format(top)))
        ub[bbidx] += max_energy

    funcDict = OrderedDict()
    funcDict['scope'] = ['SS', varnames[res1]]
    funcDict['costs'] = etable
    print(funcDict['scope'])
    funcsDict['E'+str(res1)] = funcDict
allU = []

Bidx = 0  # index of cost function in the Bs
for res1, resid1 in enumerate(positions):
    for res2 in range(res1+1, len(positions)):
        resid2 = positions[res2]
        energyl = []
        isEmpty = True
        for bbidx, bb in enumerate(myArgv):
            max_energy = float("-inf")
            binary = allB[bbidx][Bidx]

            for rot1 in range(allDomainsizes[bbidx][res1]):
                for rot2 in range(allDomainsizes[bbidx][res2]):
                    energy = binary[rot2+rot1*allDomainsizes[bbidx][res2]]
                    if energy != 0.0: isEmpty = False
                    max_energy = max(energy, max_energy)
                    energyl.append(Decimal(energyformat.format(energy)))
                dummynb = maxDomainsizes[res2] - allDomainsizes[bbidx][res2]
                for i in range(dummynb):
                    energyl.append(Decimal(energyformat.format(top)))
            dummynb = maxDomainsizes[res1]-allDomainsizes[bbidx][res1]
            for i in range(dummynb):
                energyl.extend([Decimal(energyformat.format(top))]
                               * maxDomainsizes[res2])
            ub[bbidx] += max_energy
            allB[bbidx][Bidx] = []
        Bidx += 1
        if not isEmpty:
            funcDict = OrderedDict()
            funcDict['scope'] = ['SS', varnames[res1], varnames[res2]]
            funcDict['costs'] = energyl
            print(funcDict['scope'])
            funcsDict['E'+str(res1)+"_"+str(res2)] = funcDict

header = OrderedDict()
header['name'] = 'iCFN_source'
header['mustbe'] = '<'+energyformat.format(reduce(max, ub))

cfn = OrderedDict()
cfn['problem'] = header
cfn['variables'] = varDict
cfn['functions'] = funcsDict

write_cfn(cfn, "CFN.cfn.gz", native + " 0 0.0")

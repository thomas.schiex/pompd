#!/usr/bin/env python

from __future__ import print_function
import math,numpy
import argparse
import gzip
import decimal
import os
import shutil
import re
import glob
import subprocess
from inspect import getsourcefile
from collections import OrderedDict
from decimal import Decimal
from utils import write_cfn_gzip, generatesameAA, read_dist_constraints, add_dist_constraints, add_seq_vars, read_diversity_regions,read_diversity_requirements, read_cfn_gzip
import json

import pyrosetta
from pyrosetta import toolbox
import pyrosetta.rosetta as rosetta

PARPATH = "./pars"
AAs = "ARNDCQEGHILKMFPSTWYV"
H_AAs = "AVILMFPW"
AA3to1 = {'ALA': 'A', 'ARG': 'R', 'ASN': 'N', 'ASP': 'D', 'CYS': 'C', 'GLU': 'E', 'GLN': 'Q', 'GLY': 'G', 'HIS': 'H',
          'ILE': 'I', 'LEU': 'L', 'LYS': 'K', 'MET': 'M', 'PHE': 'F', 'PRO': 'P', 'SER': 'S', 'THR': 'T', 'TRP': 'W',
          'TYR': 'Y', 'VAL': 'V'}

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def printback(m):
    print(m, end="")
    print('\b'*len(m), end="", flush=True)

def eshow(scorefunction, pose, filen):
    with open(filen, 'w') as f:
        score = scorefunction(pose)
        f.write("------------------------------------------------------------\n")
        f.write(" Scores                       Weight   Raw Score Wghtd.Score\n")
        f.write("------------------------------------------------------------\n")
        sum_weighted = 0.0
        w = scorefunction.weights()
        te = pose.energies().total_energies()
        nzwt = scorefunction.get_nonzero_weighted_scoretypes()
        for ti in range(1, len(nzwt)+1):
            name = rosetta.core.scoring.name_from_score_type(nzwt[ti])
            weight = w[nzwt[ti]]
            energy = te[nzwt[ti]]
            wenergy = weight*energy
            f.write("{:24s}".format(name)+' ')
            f.write("{:9.3f}".format(weight)+'   ')
            f.write("{:9.3f}".format(energy)+'   ')
            f.write("{:9.3f}".format(wenergy)+'\n')
            sum_weighted += te[nzwt[ti]]*w[nzwt[ti]]
        f.write("---------------------------------------------------\n")
        f.write(" Total weighted score:                           {:9.3f}".format(
            sum_weighted)+'\n')

def glob_dist_files(path):
    return glob.glob(path+".dist")+glob.glob(path+".dist?")

def glob_div_files(path):
    return glob.glob(path+".div")

def verbosify(m, l):
    if (verbose >= l):
        print(m)

# more feedback than os.system
def execute(message, commandline):
    print(message)
    print(commandline)
    proc = subprocess.call(commandline, shell=True)

def par_name(par):
    return os.path.join(PARPATH, par)

# if opt is set use it, else find in file, else use default
def arg_read(opt, fname, default):
    if (opt):
        return opt
    opt = default
    try:
        with open(par_name(fname), 'r') as argf:
            return argf.readline().strip()
    except FileNotFoundError:
        return opt

def rosetta_init():
    init_line = "-ignore_zero_occupancy false " + rotamers
    if (rosetta_flags):
        init_line += " @"+rosetta_flags
    ver = re.split('\+|\.',pyrosetta.version().split()[4])
    if (int(ver[0]) > 2020 or (int(ver[0]) == 2020 and int(ver[1]) >= 25)):
        init_line += " -packing:precompute_ig"
    if ('genpot' in scorefunction):
        init_line += " -corrections::gen_potential"
    if ('beta_nov16' in scorefunction):
        init_line += " -corrections::beta_nov16"
    if ('beta_nov15' in scorefunction):
        init_line += " -corrections::beta_nov15"
    init_line += " -out:level "+str(verbose*100)
    if fixed_seed:
        init_line += " -run:constant_seed "
    pyrosetta.init(init_line)
    global MyPyMolMover
    MyPyMolMover = pyrosetta.PyMOLMover()


############################################
# relaxes the pose nbrelax times independently using scorefunc and
# returns the lowest energy pose
def relax_backbone(pose, nbrelax, score_fxn, coordsdev):
    if (nbrelax == 0):
        return pose
    refinement = rosetta.protocols.relax.FastRelax(score_fxn)
    refinement.constrain_relax_to_start_coords(True)
    refinement.ramp_down_constraints(False)
    constrained_pose = pyrosetta.Pose()
    constrained_pose.assign(pose)
    if coordsdev > 0:
        rosetta.core.scoring.constraints.add_coordinate_constraints(constrained_pose, coordsdev, False)
        score_fxn.set_weight(rosetta.core.scoring.coordinate_constraint,1)
    decoy = pyrosetta.Pose()
    best = pyrosetta.Pose()
    best.assign(pose)
    while (nbrelax > 0):
        decoy.assign(constrained_pose)
        refinement.apply(decoy)
        if (score_fxn(decoy) < score_fxn(best)):
            best.assign(decoy)
        nbrelax -= 1
    return best

############################################
# generates a cfn JSON (OrderedDict) for pose using resfile for
# flexibility and score_fxn as the force field as well as the
# native sequence of designable/flexible positions and number of mutable residues
def generate_cfn(pose, resfile, score_fxn):
    energyformat = "{:."+str(precision)+"f}"
    ub = 0.0
    joint_ig_energy = 0.0
    prot_seq = pose.sequence()
    verbosify("\nGenerating  file", 1)
    task_design = rosetta.core.pack.task.TaskFactory.create_packer_task(pose)
    task_design.initialize_from_command_line()
    rosetta.core.pack.task.parse_resfile(pose, task_design, resfile)
    pose.update_residue_neighbors()
    verbosify("Generating packer graph", 1)

    rotsets = rosetta.core.pack.rotamer_set.RotamerSets()
    rosetta.core.pack.pack_scorefxn_pose_handshake(pose, score_fxn)
    ig = rosetta.core.pack.pack_rotamers_setup(pose, score_fxn, task_design, rotsets)
    
    for i in range(ig.get_num_nodes()):
        res = rotsets.rotamer_set_for_moltenresidue(i+1).rotamer(1)
        pose.replace_residue(rotsets.moltenres_2_resid(i+1), res, False)
    joint_energy = score_fxn(pose)

    verbosify("Extracting variables and values", 1)
    varDict = OrderedDict()
    varAADict = OrderedDict()
    varnames = []
    nativeseq = ""
    mutables = 0
    # write variables and rotamers
    for res1 in range(1, ig.get_num_nodes()+1):
        resid = rotsets.moltenres_2_resid(res1)
        natAA = prot_seq[resid-1]
        nativeseq = nativeseq + natAA
        values = []
        AAvalues = []
        mutable = False
        for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
            rotAA = rotsets.rotamer_set_for_moltenresidue(res1).rotamer(i).name1()
            if rotAA not in AAvalues: AAvalues.append(rotAA)
            if (rotAA != natAA):
                mutable = True
            values.append(rotAA+str(i-1))
        mutables += mutable
        varname = natAA+str(resid)
        varnames.append(varname)
        varDict[varname] = values
        varAADict[varname] = [r+"0" for r in AAvalues]

    verbosify("Extracting one body terms", 1)
    funcsDict = OrderedDict()

    for res1 in range(1, ig.get_num_nodes()+1):
        max_energy = -float("inf")
        etable = []
        joint_ig_energy += ig.get_one_body_energy_for_node_state(res1, 1)
        for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
            energy = ig.get_one_body_energy_for_node_state(res1, i)
            etable.append(Decimal(energyformat.format(energy)))
            max_energy = max(energy, max_energy)
        ub += max_energy
        funcDict = OrderedDict()
        funcDict['scope'] = [varnames[res1-1]]
        funcDict['costs'] = etable
        resid = rotsets.moltenres_2_resid(res1)
        funcsDict['E'+str(resid)] = funcDict

    verbosify("Extracting two bodies terms", 1)
    for res1 in range(1, ig.get_num_nodes()+1):
        resid1 = rotsets.moltenres_2_resid(res1)
        for res2 in range(res1+1, ig.get_num_nodes()+1):
            resid2 = rotsets.moltenres_2_resid(res2)
            printback("("+str(resid1)+"-"+str(resid2)+")  ")
            max_energy = float("-inf")
            if (ig.get_edge_exists(res1, res2)):
                nze = alle = 0
                energyl = []
                nzel = []
                funcDict = OrderedDict()
                funcDict['scope'] = [varnames[res1-1], varnames[res2-1]]
                joint_ig_energy += ig.get_two_body_energy_for_edge(
                    res1, res2, 1, 1)
                for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
                    for j in range(1, rotsets.rotamer_set_for_moltenresidue(res2).num_rotamers()+1):
                        energy = ig.get_two_body_energy_for_edge(
                            res1, res2, i, j)
                        max_energy = max(energy, max_energy)
                        textEnergy = energyformat.format(energy)
                        decEnergy = Decimal(textEnergy)
                        if (abs(decEnergy) != 0):
                            nzel.extend((i-1, j-1, decEnergy))
                            nze += len(textEnergy)+9
                        alle += len(textEnergy)+1
                        energyl.append(decEnergy)
                if (nze != 0):
                    if (nze+14 < alle):  # sparse
                        funcDict['defaultcost'] = 0
                        funcDict['costs'] = nzel
                    else:
                        funcDict['costs'] = energyl
                    ub += max_energy
                    funcsDict['E'+str(resid1)+"_"+str(resid2)] = funcDict

    ###Adding hydrophobicity constraints###
    if hpatch:
        exp_res = []
        exp_res = exposed_residues(pose, ig, rotsets)
        for res1 in range(0, len(exp_res)):
            resid1 = rotsets.moltenres_2_resid(res1+1)
            for ires2 in range(0, len(exp_res[res1])):
                res2 = exp_res[res1][ires2]
                resid2 = rotsets.moltenres_2_resid(res2+1)
                nze = alle = 0
                energyl = []
                nzel = []
                funcDict = OrderedDict()
                funcDict['scope'] = [varnames[res1], varnames[res2]]
                authorized_interaction = False
                for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1+1).num_rotamers()+1):
                    for j in range(1, rotsets.rotamer_set_for_moltenresidue(res2+1).num_rotamers()+1):
                        rotAA1 = rotsets.rotamer_set_for_moltenresidue(res1+1).rotamer(i).name1()
                        rotAA2 = rotsets.rotamer_set_for_moltenresidue(res2+1).rotamer(j).name1()
                        if (rotAA1 in H_AAs) and (rotAA2 in H_AAs):
                            energy = ub
                        else:
                            energy = ig.get_two_body_energy_for_edge(res1+1, res2+1, i, j)
                            authorized_interaction = True
                        textEnergy = energyformat.format(energy)
                        decEnergy = Decimal(textEnergy)
                        if (abs(decEnergy) != 0):
                            nzel.extend((i-1, j-1, decEnergy))
                            nze += len(textEnergy)+9
                        alle += len(textEnergy)+1
                        energyl.append(decEnergy)
                if (authorized_interaction):
                    if (nze != 0):
                        if (nze+14 < alle):  # sparse
                            funcDict['defaultcost'] = 0
                            funcDict['costs'] = nzel
                        else:
                            funcDict['costs'] = energyl
                        funcsDict['E'+str(resid1)+"_"+str(resid2)] = funcDict

    


    if abs(joint_energy - joint_ig_energy) > pow(10, -precision):
        template_func = OrderedDict()
        template_func['scope'] = []
        decimal_energy = Decimal(energyformat.format(
            joint_energy - joint_ig_energy))
        template_func['costs'] = [decimal_energy]
        funcsDict['E_template'] = template_func
        joint_ig_energy += float(decimal_energy)

    header = OrderedDict()
    header['name'] = pose.pdb_info().name()
    header['mustbe'] = '<'+energyformat.format(ub)

    cfn = OrderedDict()
    cfn['problem'] = header
    cfn['variables'] = varDict
    cfn['functions'] = funcsDict

    verbosify("done.", 1)
    return (cfn, nativeseq, mutables, joint_energy - joint_ig_energy)

##################################################
# extract the native and mutations from the cfn file

def getnatmut(f):
    lastthree = gzip.open(f, 'r').readline().split()[-3:]
    return (lastthree[0].decode(), int(lastthree[1]), float(lastthree[2]))

def write_full_native(filename,pose, sfx):
    nr = pose.total_residue()
    full_nat = bytearray(nr)
    for i in range(nr):
        full_nat[i] = pose.residue(i+1).name1().encode()[0]
    with open(filename, 'w') as f:
        f.write(full_nat.decode())
        f.write(" "+str(sfx(pose)))

##################################################
# Get a non trivial initial solution for upper bounding from nb_trials
# rosetta fixed backbone packing or from the relaxed native

def provide_ub(pose, pdb_file, resfile, outf, score_fxn, nb_trials):
    verbosify("\nPreparing for fixbb...", 1)
    minimum = float("inf")
    sol = ""

    for i in range(0, nb_trials):
        pose = pyrosetta.Pose()
        pyrosetta.pose_from_file(pose, pdb_file)
        task_design = rosetta.core.pack.task.TaskFactory.create_packer_task(
            pose)
        task_design.initialize_from_command_line()
        rosetta.core.pack.task.parse_resfile(pose, task_design, resfile)
        mover = rosetta.protocols.simple_moves.PackRotamersMover(
            score_fxn, task_design)
        verbosify("Fixbb run "+str(i+1), 1)
        mover.apply(pose)
        if (score_fxn(pose) < minimum):
            minimum = score_fxn(pose)
            ig = mover.ig()
            sol = ""
            for res in range(1, ig.get_num_nodes()+1):
                sol += str(ig.get_node(res).get_current_state()-1)+" "
    with open(outf, 'w') as f:
        f.write(sol+'\n')
    verbosify("done.", 1)


def exposed_residues(pose, ig, rotsets):
    copy_pose = pyrosetta.Pose()
    copy_pose.assign(pose)
    for res in range(1, copy_pose.total_residue()+1):
        pyrosetta.toolbox.mutate_residue(copy_pose, res, 'L')
    exp_res= []
    for res in range(1, ig.get_num_nodes() + 1):
        exp_res.append(rosetta.core.scoring.sasa.is_res_exposed(copy_pose,rotsets.moltenres_2_resid(res)))
    exposed_neighbors = []
    for res1 in range(1, ig.get_num_nodes()):
        cur_res_exposed_neighbors=[]
        if (exp_res[res1-1]):
            for res2 in range(res1+1 , ig.get_num_nodes()+1):
                if (ig.get_edge_exists(res1, res2)):
                    if (exp_res[res2-1]):
                        cur_res_exposed_neighbors.append(res2 - 1)
        exposed_neighbors.append(cur_res_exposed_neighbors)
    return exposed_neighbors

#################################################
# creates a pdb out_file that represents the toulbar2 solution stored
# in mat. If pwa is set to True, dumps statistics about the toulbar2
# solution and stores them in out_file. The pose must be the exact one
# that one used to create energy matrices for toulbar2.
def load_assign(pose, mat, out_file, show_file, resfile, score_fxn, pwa=False):
    copy_pose = pyrosetta.Pose()
    copy_pose.assign(pose)
    task_design = rosetta.core.pack.task.TaskFactory.create_packer_task(pose)
    task_design.initialize_from_command_line()
    rosetta.core.pack.task.parse_resfile(pose, task_design, resfile)
    pose.update_residue_neighbors()
    png = rosetta.core.pack.create_packer_graph(pose, score_fxn, task_design)
    rotsets = rosetta.core.pack.rotamer_set.RotamerSets()
    ig = rosetta.core.pack.pack_rotamers_setup(pose, score_fxn, task_design, rotsets)

    if (pwa):
        with open(out_file, 'w') as f:
            ig = rosetta.core.pack.interaction_graph.InteractionGraphFactory.create_and_initialize_two_body_interaction_graph(
                task_design, rotsets, pose, score_fxn, png)
            f.write("R1  pos Score-one R2  pos Score-one Distance  Score-two\n")
            f.write("--------------------------------------------------------\n")
            for res1 in range(1, ig.get_num_nodes()):
                nres1 = rotsets.rotamer_set_for_moltenresidue(
                    res1).rotamer(mat[res1-1]+1).name3()
                en1 = ig.get_one_body_energy_for_node_state(
                    res1, mat[res1-1]+1)
                ca1 = pose.residue(res1).xyz("CA")
                for res2 in range(res1+1, ig.get_num_nodes()+1):
                    if (ig.get_edge_exists(res1, res2)):
                        nres2 = rotsets.rotamer_set_for_moltenresidue(
                            res2).rotamer(mat[res2-1]+1).name3()
                        en2 = ig.get_one_body_energy_for_node_state(
                            res2, mat[res2-1]+1)
                        ener = ig.get_two_body_energy_for_edge(
                            res1, res2, mat[res1-1]+1, mat[res2-1]+1)
                        ca2 = pose.residue(res2).xyz("CA")
                        dist = math.sqrt(
                            pow(ca1[0]-ca2[0], 2)+pow(ca1[1]-ca2[1], 2)+pow(ca1[2]-ca2[2], 2))
                        f.write(nres1+" {:<3}".format(res1) +
                                " {:9.3f} ".format(en1))
                        f.write(nres2+" {:<3}".format(res2) +
                                " {:9.3f}".format(en2))
                        f.write(" {:9.3f}".format(dist))
                        f.write(" {:9.3f}".format(ener)+'\n')
    else:
        for i in range(0, len(mat)):
            res = rotsets.rotamer_set_for_moltenresidue(i+1).rotamer(int(mat[i]+1))
            copy_pose.replace_residue(rotsets.moltenres_2_resid(i+1), res, False)
        copy_pose.dump_pdb(out_file)
        with open(optx_file, 'w') as rotfile:
            for i in range(0, len(mat)):
                res = copy_pose.residue(i+1)
                resid = rotsets.moltenres_2_resid(i+1)
                chain = pyrosetta.rosetta.core.pose.get_chain_from_chain_id(res.chain(),copy_pose)
                if not chain.isspace():
                    rotfile.write("{}-".format(chain))
                rotfile.write("{}".format(resid))
                icode = copy_pose.pdb_info().icode(resid)
                if (icode.isspace()):
                    rotfile.write(" ")
                else:
                    rotfile.write("-{} ".format(icode))
                rotfile.write(res.name3())
                for ca in range(1, res.nchi()+1):
                    rotfile.write(" {:10.7f}".format(res.chi()[ca]))
                rotfile.write("\n")
            if (show_file) :
                eshow(score_fxn, copy_pose, show_file)
        copy_pose.pdb_info().name(opt_file)
        PyMOLvis(copy_pose)


def PyMOLvis(pose):
    MyPyMolMover.apply(pose)
    if (energy_vis != "none"):
        if (energy_vis == "full"):
            score_fxn(pose)
            MyPyMolMover.send_energy(pose)
        elif (energy_vis == "polars"):
            MyPyMolMover.send_polars(pose)
        else:
            score_fxn(pose)
            MyPyMolMover.send_energy(pose, energy_vis)
    if (hbonds_vis):
        MyPyMolMover.send_hbonds(pose)

##################################################
# optimize a pose with a 'towards native' bias
def boptimize(bias):
    [native, nmut, template] = getnatmut(cfn_file)
    print(native)
    tb2cmd = os.path.join(pypath, "toulbar2 ")+cfn_file+" -s -w="+gmec_file + \
        "-"+str(bias)+" --cpd --native="+native+" --psmbias="+str(bias)
    tb2cmd += ("| tee " if (verbose > 1) else "> ")+tb2log+"-"+str(bias)
    execute("Looking for GMEC with toulbar2", tb2cmd)
    return get_optimum(tb2log+"-"+str(bias))


##################################################
# extracts the optimum cost and GMEC sequence from toulbar2
# output. Returns Nones when no solution found.

def read_seq(tb2line):
    seq = tb2line.split()[2]
    #print(seq)
    #if len(dist_constraints) > 0:
    #    seq = seq.translate({ord(str(i)): None for i in range(10)})
    #    seq = seq[:int(len(seq)/2)]
    return(seq)

def get_optimum(tb2log):
    for line in open(tb2log):
        if "New sequence:" in line:
            seq = read_seq(line)
        if "Optimum:" in line:
            return (float(line.split()[1]), seq)
    return (None, None)

def new_search(line):
    return (re.sub('[0-9]', '', line).replace('\n','') == "+++++++++ Search for solution  +++++++++")

def get_library(tb2log):
    #diverse sequence library search:
    #Returns a list of (cost,seq) of diverse incrementally optimal sequences
    lib = []
    seq = None
    opt = None
    for line in open(tb2log):
        if "New sequence:" in line:
            seq = read_seq(line)
        elif "Optimum:" in line:
            opt = float(line.split()[1])
        elif new_search(line):
            if seq is not None:
                lib.append((opt, seq))
                seq = None
        elif "end." in line:
            if seq is not None:
                lib.append((opt,seq))
                seq = None
    return lib

##################################################
# extracts the GMEC from toulbar2 output and computes the cost
# threshold for the required energy threshold
def get_enum_threshold(tb2log, enum):
    gmec, seq = get_optimum(tb2log)
    return gmec+enum

##################################################
def mutation_number(s1, s2):
    if (len(s1) != len(s2)):
        print("You have fixed side-chains (NATRO) in your resfile.")
        print("Pass the native sequence w/o these residues using '--native'.")
        exit()
    return sum(c1 != c2 for c1, c2 in zip(s1, s2))

##################################################
def mutations(native, design):
    assert len(native) == len(design)
    return [(i+1, c2) for i, (c1, c2) in enumerate(zip(native, design)) if (c1 != c2)]

##################################################
# main
parser = argparse.ArgumentParser(
    description='Fastrelaxes a Rosetta-friendly PDB')

parser.add_argument('-i', '--input', required=True,
                    help='The input file to process (pdb or cfn)')

parser.add_argument('-H', '--hbonds', action="store_true",
                    default=False,
                    help='Select hbond energies only.')

parser.add_argument('-c', '--clashes', action="store_true",
                    default=False,
                    help='Select clashes (fa_rep) energies only.')

parser.add_argument('-f', '--nbfixbb',
                    help='Number of packer runs [default 1]')

parser.add_argument('-t', '--threshold',
                    help='Enumeration threshold [default: 0.2]')

parser.add_argument('-ne', '--nbenum',
                    help='Maximum number of suboptimal solutions to enumerate [default: 10]')

parser.add_argument('-r', '--rotamers',
                    help='Use specified rotamer set [default "-ex1 true  -ex1aro true  -ex2 true  -ex2aro false"]')

parser.add_argument('--scpseq',
                    help='Use the given sequence for side-chain packing')

parser.add_argument('-n', '--nbrelax',
                    help='Number of relaxations [default 1]')

parser.add_argument('--coordsdev', type=float,
                    default=0,
                    help='standard deviation on coordinates on backbone move constraint (relax) [default=0] (no constraint)')

parser.add_argument('-S', '--scpbranch', action="store_true",
                    default=True,
                    help='Look for a single conformation per sequence when enumerating (default: %default)')

parser.add_argument('-R', '--resfilename',
                    help='Filename of a resfile format file (see Rosetta doc, default: '+par_name("resfile")+')')

parser.add_argument('-s', '--scorefunction',
                    help='Rosetta scoring weights file w/o extension [default beta_nov16]')

parser.add_argument('--dist',
                    help='Distance constraint file')

parser.add_argument('--div',
                    help='Parameter file for diverse incremental search')

parser.add_argument('-rf', '--rosettaflags',
                    help='Rosetta flag file')

parser.add_argument('-P', '--precision',
                    help='Number of digits after decimal point in the matrix [default 6]')

parser.add_argument('-F', '--fixedseed',
                    help='Use a fixed seed for Rosetta, False means no fixed seed [default is False]')

parser.add_argument('--dorelax', action="store_true",
                    default=False,
                    help='Relax pdb and save relaxed pdb in .rlx file')

parser.add_argument('--doematrix', action="store_true",
                    default=False,
                    help='Compute energy matrix from .rlx')

parser.add_argument('--hpatch', action="store_true",
                    default=False,
                    help='Add constraints forbidding pairs of solvent accessible hydrophobic residues')

parser.add_argument('--native',
                    help='Native sequence for --doscan')

parser.add_argument('--dofixbb', action="store_true",
                    default=False,
                    help='Compute upper bound from .rlx')

parser.add_argument('--doscp', action="store_true",
                    default=False,
                    help='Produce the best conformation for the sequence')

parser.add_argument('--dogmeconf', action="store_true",
                    default=False,
                    help='Compute optimal conformation for .rlx')

parser.add_argument('--dodivlib', action="store_true",
                    default=False,
                    help='Compute optimal diverse sequence library for .rlx')

parser.add_argument('--dogmecin', action="store_true",
                    default=False,
                    help='Compute optimal conformation for .rlx without CFN file')

parser.add_argument('--dogmepose', action="store_true",
                    default=False,
                    help='Create .opt pdb file for the optimal conformation')

parser.add_argument('--dogmelibpose', action="store_true",
                    default=False,
                    help='Create .opt pdb files for the optimal diverse conformations')

parser.add_argument('--doenum', action="store_true",
                    default=False,
                    help='Compute list of optimal sequences within threshold for .cfn')

parser.add_argument('--dopairwise', action="store_true",
                    default=False,
                    help='Dump pairwise energies and distances in Angstroms in the GMEC')

parser.add_argument('--positive', action="store_true",
                    default=False,
                    help='Work on positive multistate files, using a final "+" in their suffix')

parser.add_argument('--energy_vis', action="store_true",
                    default="full",
                    help='Select energy visualisation type: "full", "none", "polars" or the name of an energy term')

parser.add_argument('--hbonds_vis', action="store_true",
                    default=False,
                    help='Activate hbonds visualisation')

parser.add_argument('-v', '--verbose',
                    help='Level of verbosity [default 1].')

args = parser.parse_args()
if (args.input[-3:] == '.gz'):
    input_file = args.input[:-3]
else:
    input_file = args.input
pos_suf = '+' if (args.positive) else ''
fixed_seed = bool(int(arg_read(args.fixedseed, "fixedseed", False)))
rlx_file = os.path.splitext(input_file)[0]+".rlx"
opt_file = os.path.splitext(input_file)[0]+".opt"+pos_suf
scp_file = os.path.splitext(input_file)[0]+".scp"
optx_file = os.path.splitext(input_file)[0]+".optx"+pos_suf
show_file = os.path.splitext(input_file)[0]+".show"+pos_suf
cfn_file = os.path.splitext(input_file)[0]+".cfn.gz"
enum_file = os.path.splitext(input_file)[0]+".enum"+pos_suf
scan_file = os.path.splitext(input_file)[0]+".scan"+pos_suf
ub_file = os.path.splitext(input_file)[0]+".conf"
gmec_file = os.path.splitext(input_file)[0]+".gmec"+pos_suf
lib_file = os.path.splitext(input_file)[0]+".lib"+pos_suf
seq_file = os.path.splitext(input_file)[0]+".seq"+pos_suf
nat_file = os.path.splitext(input_file)[0]+".nat"
wlst_file = os.path.splitext(input_file)[0]+".wlst"
tb2log = os.path.splitext(input_file)[0]+".tb2"
tb2scplog = os.path.splitext(input_file)[0]+".scp.tb2"
resfilename = os.path.splitext(input_file)[0]+".resfile"
pw_file = os.path.splitext(input_file)[0]+".pwa"+pos_suf
scorefunction = arg_read(args.scorefunction, "scorefunction", "beta_nov16")
energy_vis = arg_read(args.energy_vis, "energy_vis", "full")
hpatch = bool(int(arg_read(args.hpatch, "hpatch", 0)))
hbonds_vis = bool(int(arg_read(args.hbonds_vis, "hbonds_vis", 0)))
clashonly = args.clashes
hbondonly = args.hbonds
rotamers = arg_read(args.rotamers, "rotamers",
                    "-ex1 false  -ex1aro false  -ex2 false -ex2aro false")
scpseq =  args.scpseq
nbrelax = int(arg_read(args.nbrelax, "nbrelax", 1))
coordsdev = float(arg_read(args.coordsdev, "coordsdev", 0))
ub_trials = int(arg_read(args.nbfixbb, "nbfixbb", 1))
threshold = float(arg_read(args.threshold, "threshold", 0.2))
nbenum = (arg_read(args.nbenum, "nbenum", 10))
if (nbenum == "ALL"):
    nbenum = ""
else:
    nbenum = "="+str(int(nbenum))
scpbranch = arg_read(args.scpbranch, "sampling", True)
if (scpbranch == "ALL"):
    scpbranch = False
resfilename = args.resfilename if (args.resfilename) else resfilename
targetfile = os.path.splitext(input_file)[0]+".tgt"
precision = int(arg_read(args.precision, "precision", 6))
verbose = int(arg_read(args.verbose, "verbose", 1))
distancefiles = list(args.dist) if args.dist else glob_dist_files(os.path.splitext(input_file)[0])
diversityfiles = list(args.div) if args.div else glob_div_files(os.path.splitext(input_file)[0])
rosetta_flags = arg_read(args.rosettaflags, "rosetta_flags", "")
pypath = os.path.dirname(os.path.abspath(getsourcefile(lambda: 0)))
pose = None


if (args.dorelax or args.doematrix or args.dofixbb or args.dogmepose or args.dogmelibpose or args.dopairwise or args.dogmecin or args.doscp):
    rosetta_init()
    score_fxn = pyrosetta.create_score_function(scorefunction)

    onlyst = set()
    if (clashonly):
        onlyst.add(rosetta.core.scoring.fa_rep)
    if (hbondonly):
        #        onlyst.add(rosetta.core.scoring.hbond_sr_bb) # short range bb/bb
        #        onlyst.add(rosetta.core.scoring.hbond_lr_bb) # long range bb/bb
        onlyst.add(rosetta.core.scoring.hbond_bb_sc)
        #        onlyst.add(rosetta.core.scoring.hbond_sc)
    if (onlyst):
        nzwt = score_fxn.get_nonzero_weighted_scoretypes()
        for ti in range(1, len(nzwt)+1):
            if nzwt[ti] not in onlyst:
                print("Turning", nzwt[ti], "off")
                score_fxn.set_weight(nzwt[ti], 0.0)
    
    dist_constraints = read_dist_constraints(distancefiles)

if (args.dorelax):
    pose = pyrosetta.Pose()
    verbosify("\nReading PDB file...", 1)
    pyrosetta.pose_from_file(pose, input_file)

    verbosify("done.", 1)
    PyMOLvis(pose)
    if (nbrelax > 0):
        verbosify("Relaxing...", 1)
        pose = relax_backbone(pose, nbrelax, score_fxn, coordsdev)
        verbosify("done.", 1)
        verbosify("Saving relaxed PDB in "+rlx_file, 1)
        pose.dump_pdb(rlx_file)
        verbosify("done.", 1)
        pose.pdb_info().name(rlx_file)
        PyMOLvis(pose)
    else:
        shutil.copy(input_file, rlx_file)
        pose.pdb_info().name(rlx_file)
        PyMOLvis(pose)


####################
if (args.doematrix):
    if (pose is None):
        pose = pyrosetta.Pose()
        verbosify("\nReading PDB file...", 1)
        pyrosetta.pose_from_file(pose, rlx_file)
        pose.pdb_info().name(rlx_file)
        verbosify("done.", 1)
        write_full_native(nat_file,pose,score_fxn)

    (cfn, native, mutables, template) = generate_cfn(pose, resfilename, score_fxn)

    list_div_pos = []
    if len(diversityfiles) > 0:
        if len(diversityfiles) > 1:
            print("Too many diversity instruction files for incremental search")
            exit(1)
        else:
            list_div_pos = read_diversity_regions(diversityfiles[0],pose)
            
    add_seq_vars(cfn,list_div_pos)
    
    if len(dist_constraints) > 0:
        add_dist_constraints(pose, cfn, dist_constraints)
    write_cfn_gzip(cfn, cfn_file, native + " " +
                   str(mutables) + " " + str(template))

####################
if (args.dofixbb):
    if (pose is None):
        pose = pyrosetta.Pose()
        verbosify("\nReading PDB file...", 1)
        pyrosetta.pose_from_file(pose, rlx_file)
        pose.pdb_info().name(rlx_file)
        verbosify("done.", 1)
    if (ub_trials > 0):
        provide_ub(pose, rlx_file, resfilename, ub_file, score_fxn, ub_trials)

##################################
if (args.doscp):
    tb2cmd = os.path.join(pypath, "toulbar2")+" "+cfn_file + \
        " -dee: -m -A -s -w=" + \
        scp_file+" --cpd --mut=0:"+scpseq
    tb2cmd += (" | tee " if (verbose > 1) else " > ")+tb2scplog
    execute("Looking for optimal side-chain orientations with toulbar2", tb2cmd)
    if (pose is None):
        pose = pyrosetta.Pose()
        verbosify("\nReading PDB file...", 1)
        pyrosetta.pose_from_file(pose, rlx_file)
        verbosify("done.", 1)
    with open(scp_file) as f:
        for last in f:
            pass
        conf = [int(x) for x in last.split()]
    verbosify("Generating optimized side-chain conformation for " +
    pose.pdb_info().name()[:-4] + " with sequence '"+scpseq+"'", 1)
    load_assign(pose, conf, scp_file, show_file, resfilename, score_fxn)
    verbosify("done.", 1)
        
# TODO -ub toulbar2 should be a dcost
##################################
if (args.dogmeconf):
    if (os.path.isfile(ub_file)):
        shutil.copyfile(ub_file, gmec_file)
    else:
        ub_file = ""
    
    dist_constraints = read_dist_constraints(distancefiles)

    tb2cmd = os.path.join(pypath, "toulbar2")+" --sol_ext='.conf' "+cfn_file + \
        " "+ub_file+" -dee: -A -O=-3 -B=1 -s -w=" + gmec_file+" --cpd"
    
    tb2cmd += (" | tee " if (verbose > 1) else " > ")+tb2log

    execute("Looking for GMEC with toulbar2", tb2cmd)
    gmec, gmecseq = get_optimum(tb2log)
    [native, nmut, template] = getnatmut(cfn_file)
    with open(seq_file, 'w') as f:
        f.write(gmecseq+" "+str(gmec))
        verbosify("GMEC at "+str(gmec+template), 1)
        verbosify("Optimal sequence is "+gmecseq, 1)
        
if (args.dodivlib):
    if (os.path.isfile(ub_file)):
        shutil.copyfile(ub_file, lib_file)
    else:
        ub_file = ""
    
    dist_constraints = read_dist_constraints(distancefiles)

    tb2cmd = os.path.join(pypath, "toulbar2")+" --sol_ext='.conf' "+cfn_file + \
        " "+ub_file+" -dee: -A -s -w=" + lib_file+" --cpd"
    
    diversityfiles = glob_div_files("*")

    if len(diversityfiles) > 0:
        if len(diversityfiles) > 1:
            print("Too many diversity instruction files for incremental search")
            exit(1)
        else:
            (diversity, nsol) = read_diversity_requirements(diversityfiles[0])
        tb2cmd += f' -m -hbfs: -a={nsol} -div={diversity}'
    else:
        tb2cmd += ' -O=-3 -B=1'
       
    tb2cmd += (" | tee " if (verbose > 1) else " > ")+tb2log
    if (True):
        execute("Looking for optimal diverse sequence library with toulbar2", tb2cmd)
        divlib = get_library(tb2log)
        [native, nmut, template] = getnatmut(cfn_file)
        with open(seq_file, 'w') as f:
            for i, (gmec, gmecseq) in enumerate(divlib):
                f.write(gmecseq+" "+str(gmec)+'\n')
                if i == 0:
                    verbosify("GMEC at "+str(gmec+template), 1)
                    verbosify("Optimal sequence is "+gmecseq, 1)

##################################
if (args.dogmecin):
    if (pose is None):
        pose = pyrosetta.Pose()
        verbosify("\nReading PDB file...", 1)
        pyrosetta.pose_from_file(pose, rlx_file)
        pose.pdb_info().name(rlx_file)
        verbosify("done.", 1)
    theCFN = generate_CFN(pose, resfilename, score_fxn)
    sol = theCFN.Solve()
    with open(seq_file, 'w') as f:
        f.write(" ".join([str(i) for i in sol]))

##################################
if (args.dogmepose or args.dopairwise):
    if (pose is None):
        pose = pyrosetta.Pose()
        verbosify("\nReading PDB file...", 1)
        pyrosetta.pose_from_file(pose, rlx_file)
        pose.pdb_info().name(rlx_file)
        verbosify("done.", 1)

    rot_positions = []
    cfn = read_cfn_gzip(cfn_file)
    for var in cfn["variables"].keys():
        if var[0] in AAs:
            rot_positions.append(int(var[1:])-1)

    with open(gmec_file) as f:
        for last in f:
            pass
        conf = [int(x) for x in last.split()]
        conf = [conf[i] for i in rot_positions]
    if (args.dogmepose):
        verbosify("Generating PDB and energy report for " +
                  pose.pdb_info().name()[:-4] + " in conformation "+gmec_file, 1)
        load_assign(pose, conf, opt_file, show_file, resfilename, score_fxn)
        verbosify("done.", 1)
    if (args.dopairwise):
        verbosify("Generating pairwise analysis for " +
                  pose.pdb_info().name()[:-4] + " in conformation "+gmec_file, 1)
        load_assign(pose, conf, pw_file, None, resfilename, score_fxn, True)
        verbosify("done.", 1)

##################################
if (args.dogmelibpose):
    if (pose is None):
        pose = pyrosetta.Pose()
        verbosify("\nReading PDB file...", 1)
        pyrosetta.pose_from_file(pose, rlx_file)
        pose.pdb_info().name(rlx_file)
        verbosify("done.", 1)

    rot_positions = []
    cfn = read_cfn_gzip(cfn_file)
    for var in cfn["variables"].keys():
        if var[0] in AAs:
            rot_positions.append(int(var[1:])-1)
    with open(lib_file, 'r') as f:
        confs = [l.split() for l in f.readlines()]

    verbosify("Generating PDBs and energy reports for " +
              pose.pdb_info().name()[:-4] + " in conformation library "+lib_file, 1)
    for i, conf in enumerate(confs):
        conf = [int(conf[i]) for i in rot_positions]
        load_assign(pose, conf, opt_file+str(i), show_file+str(i), resfilename, score_fxn)
    verbosify("done.", 1)

# TODO threshold unit
if (args.doenum):
    dist_constraints = read_dist_constraints(distancefiles)
    tb2threshold = get_enum_threshold(tb2log, threshold)
    enumcmd = os.path.join(pypath, "toulbar2 ")+cfn_file + \
        " -a"+nbenum+" -ub="+str(tb2threshold) + \
        " -dee: -A -s -hbfs: --cpd "
    if scpbranch:
        enumcmd += "--scpbranch "
    enumcmd += "| grep -w -e 'New sequence:' -e 'New rotamers' "
    enumcmd += ("| tee " if (verbose) else "> ")+enum_file
    execute("Enumerating suboptimal sequences with toulbar2", enumcmd)

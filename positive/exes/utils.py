#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 16:52:50 2018

@author: mruffini
"""

import numpy as np
import gzip
import re
import sys
import os
import simplejson as json
from collections import OrderedDict
from json import encoder
import decimal
import subprocess
import itertools
from decimal import Decimal

AAs = "ARNDCQEGHILKMFPSTWYV"
n_aa = len(AAs)
AA3to1 = {'ALA': 'A', 'ARG': 'R', 'ASN': 'N', 'ASP': 'D', 'CYS': 'C', 'GLU': 'E', 'GLN': 'Q', 'GLY': 'G', 'HIS': 'H',
          'ILE': 'I', 'LEU': 'L', 'LYS': 'K', 'MET': 'M', 'PHE': 'F', 'PRO': 'P', 'SER': 'S', 'THR': 'T', 'TRP': 'W',
          'TYR': 'Y', 'VAL': 'V'}


########################

# generate "sameAA" constraint


def generatesameAA(l1, l2, precision):
    ltuples = []
    energyformat = "{:."+str(precision)+"f}"
    aas = set(v[0] for v in l1).intersection(set(v[0] for v in l2))
    for aa in aas:
        lrot1 = [i for i, x in enumerate(l1) if x[0] == aa]
        lrot2 = [i for i, x in enumerate(l2) if x[0] == aa]
        for val1, val2 in itertools.product(lrot1, lrot2):
            ltuples.append(val1)
            ltuples.append(val2)
            ltuples.append(Decimal(energyformat.format(0)))
    return ltuples


def read_cfn_gzip(cfn_filename):
    file = gzip.open(cfn_filename, 'r')
    first_byte = file.peek(1)
    if first_byte.decode("utf-8")[0] == '#':
        file.readline()
    cfn = json.load(file, object_pairs_hook=OrderedDict, use_decimal=True)
    return cfn


def read_cfn(cfn_filename):
    file = open(cfn_filename, 'r')
    first_byte = file.peek(1)
    if first_byte.decode("utf-8")[0] == '#':
        file.readline()
    cfn = json.load(file, object_pairs_hook=OrderedDict, use_decimal=True)
    return cfn


def read_sols(sol_filename):
    sols = []
    if sol_filename == None:
        return sols
    else:
        sol_file = open(sol_filename, 'r')
        for sol in sol_file:
            sol = list(map(int, sol.rstrip().split(' ')))
            sols.append(sol)
        return sols


def sols_to_cpd_sols(sols, cfn):
    for sol in sols:
        for pos in range(len(sol)):
            sol[pos] = list(cfn['variables'].values())[pos][sol[pos]][0]


def write_cfn(cfn, output_filename, comment="", indent=0):
    cfn_str = json.dumps(cfn, indent=indent,
                         separators=(',', ':'), use_decimal=True)
    with open(output_filename, 'w') as fout:
        if (comment):
            fout.write('# '+comment+'\n')
        fout.write(cfn_str)


def write_cfn_gzip(cfn, output_filename, comment="", indent=0):
    cfn_str = json.dumps(cfn, indent=indent,
                         separators=(',', ':'), use_decimal=True)
    cfn_bytes = cfn_str.encode('utf-8')
    with gzip.GzipFile(output_filename, 'w') as fout:
        if (comment):
            fout.write(('# '+comment+'\n').encode('utf-8'))
        fout.write(cfn_bytes)


def read_sim_mat(msim_filename):
    msim = []
    msim_file = open(msim_filename, 'r')
    for line in msim_file:
        line = line.split(' ')
        line = [int(a) for a in line if(a != '' and a != '\n')]
        msim.append(line)
    return msim


def dissim(val1, val2, val_list=None, msim=None):
    if val1 == val2:
        return 0
    else:
        if msim == None:
            return 1
        else:
            i1 = val_list.index(val1)
            i2 = val_list.index(val2)
            return(msim[i1][i1] - msim[i1][i2])


def get_domain(var, cfn):
    if isinstance(cfn['variables'][var], int):
        domain = range(0, abs(cfn['variables'][var]))
    else:
        domain = list(cfn['variables'][var])
    return domain


def get_aa_domain(var, cfn):  # for sequence variables in cpd
    aa_domain = []

    for rot in cfn['variables'][var]:
        if rot[0] not in aa_domain:
            aa_domain.append(rot[0])
    return aa_domain


def add_seq_vars(cfn,list_div_pos):
    """
    Adds sequence variables with domain = possible amino acids at the corresponding position
    And the necessary binary cost functions to ensure that rotamer and sequence variables correspond to the same aa
    Positions in list_div_pos use $ special character for toulbar2 diversity requirements
    """
    rot_vars = list(cfn['variables'].keys())
    for var in rot_vars:
        domain = get_aa_domain(var, cfn)
        pos = int(var[1:])
        seq_var_name = ('$' if pos in list_div_pos else 'Z')+var[1:]
        if len(domain) > 1:
            cfn['variables'][seq_var_name] = domain
            # Extra binary cost functions to ensure that rotamer and sequence variables correspond to the same aa
            fname = 'fseq' + str(var) + '_' + str(seq_var_name)
            cfn['functions'][fname] = OrderedDict()
            cfn['functions'][fname]['scope'] = [var, seq_var_name]
            cfn['functions'][fname]['defaultcost'] = float(cfn['problem']['mustbe'][1:])
            cfn['functions'][fname]['costs'] = []
            for rot in cfn['variables'][var]:
                for aa in cfn['variables'][seq_var_name]:
                    if rot[0] == aa[0]:
                        cfn['functions'][fname]['costs'] += [rot, aa, 0]
    return cfn

def execute(message, commandline):
    print(message)
    print(commandline)
    proc = subprocess.call(commandline, shell=True)


def get_optimum(tb2log):
    lines = open(tb2log).readlines()
    for i in range(len(lines)):
        if "New solution:" in lines[i]:
            seq = re.split(' ', lines[i + 1])[1:]
            for pos in range(len(seq)):
                seq[pos] = int(seq[pos])
        if "Optimum:" in lines[i]:
            s = re.split(' ', lines[i])
            return (float(s[1]), seq)
    return (None, None)



def read_diversity_regions(diversityfilename, pose):
    with open(diversityfilename) as file:
        wishes = json.load(file)
        list_positions = []
        for region in wishes["regions"]:
            region = region.split()
            chain = region[-1]
            pos = [int(p) for p in region[0].split('-')]
            if len(pos) > 2:
                print("Wrong number of positions in diversity region:", region)
                sys.exit(1)
            pos_init = pose.pdb_info().pdb2pose(chain, pos[0])
            if len(pos) == 2:
                pos_end = pose.pdb_info().pdb2pose(chain, pos[-1])
            else:
                pos_end = pose.pdb_info().pdb2pose(chain, pos[0])
            list_positions.extend(list(range(pos_init, pos_end+1)))
    return list_positions

def read_diversity_requirements(diversityfilename):
    with open(diversityfilename) as file:
        wishes = json.load(file)
        diversity = int(wishes["diversity"])
        nsol = int(wishes["solutions"])
    return (diversity, nsol)

def read_dist_constraints(distancefiles):
    constraints = []
    for filename in distancefiles:
        file = open(filename, 'r')
        #first_byte = file.peek(1)
        # if first_byte.decode("utf-8")[0] == '#':
        #    file.readline()
        constraint = json.load(file, object_pairs_hook=OrderedDict)
        constraints.append(constraint)
        file.close()
    return constraints


def counting_bin_dissim(cfn, vars_list, target, name, min_mut, max_mut, val_list=None, msim=None):
    n_vars = len(vars_list)
    #print(f'constraint : {target} {min_mut} {max_mut} {n_vars}')
    if len(target) != n_vars:
        print("Wrong target sequence length in distance constraints")
        sys.exit(1)
    q = 'Z_q' + name
    cfn['variables'][q + 'init'] = 1
    c = 'Z_c' + name
    max_dissim = 1
    if msim is not None:
        max_dissim = max([max(l) for l in msim])
    if max_mut < n_vars * max_dissim:
        dist = max_mut
    else:
        dist = min_mut
    qi = q
    for i in range(n_vars):
        var = vars_list[i]
        domain = get_domain(var, cfn)
        qi = q + str(i)
        cfn['variables'][qi] = dist + 1 # 0 1 2 .. min_mut ... max_mut max_mut+1 avec max_mut+1 toujours interdit
        if i == 0:
            qip = q+'init'
        else:
            qip = q + str(i - 1)
        ci = c + str(i)
        cfn['variables'][ci] = (max_dissim + 1) * (dist + 1)
        fca = f'f_{ci}_{var}'
        cfn['functions'][fca] = OrderedDict()
        cfn['functions'][fca]['scope'] = [ci, var]
        cfn['functions'][fca]['defaultcost'] = float(
            cfn['problem']['mustbe'][1:])
        cfn['functions'][fca]['costs'] = []
        fcqi = f'f_{ci}_{qi}'
        cfn['functions'][fcqi] = OrderedDict()
        cfn['functions'][fcqi]['scope'] = [ci, qi]
        cfn['functions'][fcqi]['defaultcost'] = float(
            cfn['problem']['mustbe'][1:])
        cfn['functions'][fcqi]['costs'] = []
        fcqip = f'f_{ci}_{qip}'
        cfn['functions'][fcqip] = OrderedDict()
        cfn['functions'][fcqip]['scope'] = [ci, qip]
        cfn['functions'][fcqip]['defaultcost'] = float(
            cfn['problem']['mustbe'][1:])
        cfn['functions'][fcqip]['costs'] = []
        delta_seen = []
        for val_index in range(len(domain)):
            val = domain[val_index]
            delta = dissim(val, target[i], val_list, msim)
            qip_add = False
            if delta not in delta_seen:
                qip_add = True
                delta_seen.append(delta)
            for qip_val in range(len(get_domain(qip, cfn))):
                if max_mut >= n_vars*max_dissim:
                    qi_val = min(qip_val + delta, dist)
                else:
                    qi_val = qip_val + delta
                if qi_val <= dist:
                    ci_val = delta * (dist + 1) + qip_val
                    cfn['functions'][fca]['costs'] += [ci_val, val_index, 0]
                    if qip_add:
                        cfn['functions'][fcqi]['costs'] += [ci_val, qi_val, 0]
                        cfn['functions'][fcqip]['costs'] += [ci_val, qip_val, 0]
    # Adding unary cost on Qn
    if (n_vars == 0):
        qi = q+'init'
    cfn['functions']['unary_' + qi] = OrderedDict()
    cfn['functions']['unary_' + qi]['scope'] = [qi]
    cfn['functions']['unary_' + qi]['costs'] = []
    for j in range(min_mut):
        cfn['functions']['unary_' + qi]['costs'].append(float(cfn['problem']['mustbe'][1:]))
    if dist == min_mut:
        cfn['functions']['unary_' + qi]['costs'].append(0)
    else:
        for j in range(min_mut, max_mut+1):
            cfn['functions']['unary_' + qi]['costs'].append(0)


def seqvar_position(seq_var_name):
    return int(seq_var_name.split('_')[1][1:])

def add_dist_constraints(pose, cfn, constraints):
    n_vars = len(cfn['variables'])
    seq_vars = [x for x in cfn['variables'].keys() if x[0] == 'Z']
    #print (seq_vars)
    for i in range(len(constraints)):
        constraint = constraints[i]
        #var_list
        positions = []
        for region in constraint["regions"]:
            region = region.split(' ')
            chain = region[-1]
            pos = [int(p) for p in region[0].split('-')]
            pos_init = pose.pdb_info().pdb2pose(chain, pos[0])
            pos_end = pose.pdb_info().pdb2pose(chain, pos[-1])
            positions += list(range(pos_init, pos_end+1))
            #print(f'{region} {pos_init} {pos_end}')
        var_list = [x for x in seq_vars if int(x[1:]) in positions]
        #print(var_list)
        #target
        if constraint["target"][-6:] == '.fasta':
            if os.path.exists(constraint["target"]):
                with open(constraint["target"]) as f:
                    target = f.readlines()[1].replace('\n', '')
            else:
                print(f'File {constraint["target"]} does not exist (distance constraint)')
                sys.exit(1)
        else:
            if any([x not in AAs for x in constraint["target"]]) or (len(constraint["target"]) != pose.total_residue()):
                print(f'Invalid target sequence in distance constraint')
                sys.exit(1)
            target = constraint["target"]
        target_pruned = [t for i,t in enumerate(target) if f'Z{i+1}' in var_list] # assuming positions start at 1 in pdb sequence          
        #name
        name = f'distconstr{i}'
        #min_mut
        min_mut = constraint["min_mut"]
        # max_mut
        max_mut = constraint["max_mut"]
        counting_bin_dissim(cfn, var_list, target_pruned,
                            name, min_mut, max_mut)

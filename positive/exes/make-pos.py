#!/usr/bin/env python

from __future__ import print_function
import math
import argparse
import os
import itertools
import gzip
import json
from functools import reduce
from collections import OrderedDict
from utils import write_cfn_gzip, read_cfn_gzip, generatesameAA, AAs
from decimal import Decimal

PARPATH = "./pars"


# weight file: attributing weigths to different conformations (states)

def readweights(conf):
    default=1
    try:
        with open(conf[:-3]+"weight", 'r') as wf:
            return int(wf.readline())
    except FileNotFoundError:
        return default

##########################################################
# main
parser = argparse.ArgumentParser(
    description='Merges .cfn.gz CPD matrices for positive multistate design.')
parser.add_argument('matrices',  nargs='+',
                    help='sequence of .cfn.gz matrices')

parser.add_argument('-o', '--output', help='Output .cfn.gz file')

args = parser.parse_args()
outfile = args.output if args.output else 'positive.cfn.gz'
listfile = os.path.splitext(os.path.splitext(outfile)[0])[0]+'.wlst'
matricen = args.matrices

if (len(matricen) < 2):
    print("We need at least two matrices to merge")
    exit(1)

natmuts = [gzip.open(f, 'r').readline().split()[-3:] for f in matricen]
cfns = [read_cfn_gzip(f) for f in matricen]
nmut = reduce(lambda a, b: a if a == b else -1, [int(l[1]) for l in natmuts])

#if (nmut == -1):
#    print("All designs do not have the same number of mutable positions, exiting")
#    exit(1)

pos_natmut = reduce(
    lambda a, b: [a[0]+b[0], int(a[1])+int(b[1]), float(a[2])+float(b[2])], natmuts)

# build problem description
tops = [cfn['problem']['mustbe'][1:] for cfn in cfns]
max_prec = max([(len(top)-top.find('.')-1) for top in tops])
energy_format = "{:."+str(max_prec)+"f}"
pos_top = sum([Decimal(top) for top in tops])

header = OrderedDict()
header["name"] = '+'.join([cfn['problem']['name'] for cfn in cfns])
header["mustbe"] = "<"+energy_format.format(pos_top)


##CFNs with sequence variables, sameAA constraints, and distance constraints.
# CFNs are concatenated - sequence variables are merged


# build set of variables

def new_var_name(name, idx):
    if name[0] == '$':
        return name
    elif (name[0] == 'Z' and name[:2] != 'Z_'):
        for cfn in cfns:
            if '$'+name[1:] in cfn["variables"].keys():
                return '$'+name[1:]
        else:
            return name
    else:
        return name + '_' + str(idx)

pos_variables = OrderedDict()
pos_varoffset = []
pos_varsize = []
var_offset = 0
for idx, cfn in enumerate(cfns):
    n_rotvars = len([x for x in cfn["variables"].keys() if x[0] in AAs])
    pos_varsize.append(n_rotvars)
    pos_varoffset.append(var_offset)
    for name, domain in cfn['variables'].items():
        if name[0] == "$" or (name[0] == 'Z' and name[:2] != 'Z_'):
            if name in pos_variables.keys():
                pos_variables[name] = list(set(domain + pos_variables[name]))
            elif name[0] == '$' and 'Z'+name[1:] in pos_variables.keys():
                pos_variables.pop('Z'+name[1:])
                pos_variables[name] = domain
            elif name[0] == 'Z' and '$'+name[1:] in pos_variables.keys():
                pos_variables['$'+name[1:]] = list(set(domain + pos_variables['$'+name[1:]]))
            else:
                pos_variables[name] = domain
                var_offset += 1
        else:
            pos_variables[new_var_name(name,idx)] = domain
            var_offset += 1

# build functions, first original ones
pos_functions = OrderedDict()
for idx, cfn in enumerate(cfns):
    weight=readweights(cfn['problem']['name'])
    for name, function in cfn['functions'].items():
        if idx == 0 or 'fseq' not in name:
            scope = [new_var_name(x,idx) for x in function['scope']]
            function['scope'] = scope
            if "fseq" in name and 'defaultcost' in function:
                function['defaultcost'] = energy_format.format(pos_top)
            if (weight!=1):
                if ("defaultcost" in function):
                    cost_pos=len(function['scope'])+1
                    costs = [c*weight if ((idx+1)%cost_pos==0) else c for idx, c in enumerate(function['costs'])]
                else:
                    costs = [c*weight for c in function['costs']]
                function['costs'] = costs
            pos_functions[name+'_'+str(idx)] = function

# write offset file for solution dissection
with open(listfile, 'w') as offile:
    json.dump(dict(zip(matricen, zip(pos_varoffset, pos_varsize))), offile)

# get mutable variables
mutables = []
for idx, cfn in enumerate(cfns):
    mutable = []
    for (name, domain) in cfn['variables'].items():
        if name[0] != 'Z' and name[0] != '$':
            flex=reduce(lambda a, b: a[0] if a[0]==b[0] else str(-1), domain)
            if (flex==str(-1)):
                mutable.append((name, domain))
    mutables.append(mutable)

# create all AA useful equality constraints, we need only a chain of these.
for idx in range(len(cfns)-1):
    for (name1, domain1), (name2, domain2) in zip(mutables[idx], mutables[idx+1]):
        AA1 = set([r[0] for r in domain1])
        AA2 = set([r[0] for r in domain2])
        allAAs = AA1.union(AA2)
        if (len(allAAs) > 1):
            sameAA = OrderedDict()
            sameAA['scope'] = [name1+'_'+str(idx), name2+'_'+str(idx+1)]
            sameAA['defaultcost'] = energy_format.format(pos_top)
            sameAA['costs'] = generatesameAA(domain1, domain2, max_prec)
            pos_functions['same_seq_'+name1+'_' +
                          str(idx)+name2+'_'+str(idx+1)] = sameAA

pos_cfn = OrderedDict()
pos_cfn['problem'] = header
pos_cfn['variables'] = pos_variables
pos_cfn['functions'] = pos_functions

write_cfn_gzip(pos_cfn, outfile, pos_natmut[0].decode("utf-8") +
          " " + str(pos_natmut[1]) + " " + str(pos_natmut[2]))

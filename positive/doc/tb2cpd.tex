\documentclass{tufte-handout}

\usepackage{hyperref}

\author{T. Schiex, D. Simoncini, J. Vucinic} 
\title{Pomp$^d$: Optimal Conformations \&\
  Sequences for POsitive Multistate Protein $d$esign}

\begin{document}
\maketitle

\section{Introduction}

\newthought{This package}\cite[-10em]{vucinic2019} allows to compute
guaranteed global minimum energy conformations and sequences adapted
to one or several rigid backbones defined by Rosetta-friendly PDBs
file\marginnote[-1em]{$\rightarrow$ \texttt{.pdb} extension} and a
description of possible mutations and conformations in
\texttt{Rosetta} resfiles\marginnote{$\rightarrow$ \texttt{.resfile}
  extension}. \texttt{Rosetta} provides the force field and rotamer
library, \texttt{toulbar2} the guaranteed\marginnote{See
  \href{http://www.inra.fr/mia/T/toulbar2}{\url{www.inra.fr/mia/T/toulbar2}}}
optimization\cite{toulbar2}.

Given suitable time, the script can exhaustively enumerate sequences
(and conformations) in the neighborhood of the optimum for several
backbones. Computations are naively parallelized through
\texttt{make}. The optimization step is worst-case exponential but
both single-state and positive multi-state full redesigns of 100
amino-acids are usually feasible.

\section{How can I install it ?}

\newthought{You must decide} between a python 2.7 or a python 3.x
install. If you decide for python 2.7, use \texttt{python2} instead of
\texttt{python3} in all command lines below.

\begin{enumerate}
\item you must have \href{http://www.pyrosetta.org/dow}{PyRosetta4}
  installed. You can get a licence and download PyRosetta4 from
  \href{http://www.pyrosetta.org/dow}{\url{http://www.pyrosetta.org/dow}}.
  
  To install PyRosetta4, you'll need to have the
  \texttt{python-setuptools} or \texttt{python3-setuptools}
  installed. Extract the chosen PyRosetta4 archive, go in the
  \texttt{setup} directory and type ``\texttt{sudo python3 setup.py
    install} (system-wide install)'' or ``\texttt{python3 setup.py
    install --user}'' (user install).
  
\item you must have the following executables accessible in the
  executable PATH: \texttt{git}, \texttt{make}, \texttt{cmake} and a
  \texttt{c++} compiler\marginnote{C++11 compatible compiler
    required}.
  
\item You must have the following libraries and header files
  installed:\marginnote{GNU Multiprecision and Boost Graph libraries
    and file compression libs.}  \texttt{libgmp-dev},
  \texttt{libboost-iostreams-dev}, \texttt{libboost-graph-dev},
  \texttt{zlib1g-dev} and \texttt{liblzma-dev}.
\end{enumerate}

The package will download and locally install \texttt{toulbar2} on
first usage.\marginnote[0em]{Try \texttt{make toulbar2} to have it
	done now.}

If you want to make this package available system-wide, copy it in
your favorite location and edit the \texttt{Makefile} to set the
\verb|INSTALLPATH| to this location. The \texttt{Makefile} can then be
copied anywhere and will work with local \texttt{pdb} files and
parameters using the system-wide installed executables.

\section{How does it work ?}
\label{work}

We rely on \texttt{PyRosetta4}, \texttt{toulbar2}, Python
scripts\marginnote[-1em]{The scripts are in the \texttt{exes}
  directory and parameters in \texttt{pars}.}, a \texttt{Makefile} and
a set of parameter files. The \texttt{Makefile} is configured for
\texttt{python3}. If you have \texttt{PyRosetta4} installed for
\texttt{python-2.7}, you'll have to edit the \texttt{PYTHON} variable
at the top of the \texttt{Makefile}.

If you want to use PyMol as a visualizer during your design, please do
a \texttt{make pymolserver}. This will install a PyMol script that
allows for communication with PyMol. Just launch PyMol before anything
else.

\subsection{Single state design}

The \texttt{Makefile} can generate optimal sequence/conformations and
structures for each PDB independently. Sequences are represented with
one letter amino-acid codes, the conformations are sequences of
rotamer numbers that can be used by the \texttt{Makefile} to generate
PDB structures.

For each initial PDB, the \texttt{Makefile} can generate:
\begin{itemize}
\item relaxed PDBs suitable for the selected energy
  function\marginnote{$\rightarrow$\texttt{.rlx} suffix}.
\item low energy conformations produced by \texttt{Rosetta}
  \texttt{fixbb} protocol. May speed up guaranteed GMEC
  computation\marginnote{$\rightarrow$\texttt{.conf} suffix}.
\item energy matrices for the relaxed
  structures\marginnote{$\rightarrow$\texttt{.cfn.gz} suffixes}.
\item an optimal conformation for each relaxed PDB produced
  using toulbar2, an optimal sequence and a log file
  \marginnote{$\rightarrow$\texttt{.gmec}, \texttt{.seq},
    \texttt{.tb2} suffixes}.
\item a library of diverse optimal conformation for each relaxed PDB produced
  using toulbar2, a library of optimal sequences and a log file
  \marginnote{$\rightarrow$\texttt{.lib}, \texttt{.seq},
    \texttt{.tb2} suffixes}.
\item an optimal sequence for each relaxed PDB produced using
  toulbar2, and log file \marginnote{$\rightarrow$\texttt{.e.gmec},
    \texttt{.e.seq}, \texttt{.e.tb2} suffixes} based solely on the
  multiple alignment.
\item an optimal PDB structure for the relaxed backbones and associated
  energy decomposed by type\marginnote{$\rightarrow$\texttt{.opt} and
    \texttt{.show} suffix}.
\item a library of diverse optimal PDB structures for the relaxed backbones and associated
  energy decomposed by type\marginnote{$\rightarrow$\texttt{.optlib} and
    \texttt{.showlib} suffix}.
\item a dump of all pairwise energies and distances between residues
  in the GMECs\marginnote{$\rightarrow$\texttt{.pwa} suffix}.
\item a (possibly exhaustive) list of sequences (and possibly
  conformation counts) within an energy threshold of the
  GMECs\marginnote{$\rightarrow$\texttt{.enum} suffix}.
\item a sequence
\end{itemize}

\subsection{Positive multi-state design}

The \texttt{Makefile} can also generate a sequence (and suboptimal
sequences) such that the sum of the energies of the optimal
conformation of this sequence on each backbone is
minimum. Equivalently, this identifies sequences that have a minimum
average energy on all states, where the energy of a sequence is the
energy of its minimum energy conformation. These are the positive
multistate designs. Each backbone/resfile must have the same number of
mutable residues. It is assumed that the order of the mutable residues
in the different backbones is the same (the first mutable residue
represent the same residue, the second too,\ldots).

For such multistate designs, the script can generate:
\begin{enumerate}
\item \texttt{positive.cfn.gz}: a representation of the problem
  of\marginnote{\texttt{positive.cfn.gz}} finding a sequence that best
  maps to all relaxed backbones. Formally, we look for a sequence that
  minimizes the following criteria: the sum on all backbones of the
  energy of its best conformation.
\item \texttt{positive.gmec}: the collection
  of\marginnote{\texttt{positive.gmec}, \texttt{positive.seq}}
  conformations and sequences that optimize the previous
  criteria as well. The sequence contains the concatenation of
  all sequences over all states (repeated).
\item \texttt{positive.lib}: a diverse library of the collections
  of\marginnote{\texttt{positive.lib}, \texttt{positive.seq}}
  conformations and sequences that optimize the previous
  criteria as well. A sequence contains the concatenation of
  all sequences over all states (repeated).
\item \texttt{positive.enum}: a list of sequences that are within a
  threshold of the optimal sequence.
\end{enumerate}

These can be projected back on the original backbones to get:
\begin{itemize}
\item the optimal conformation of the jointly optimal sequence
  for each relaxed PDB
  \marginnote{$\rightarrow$\texttt{.gmec+} suffixes}.
\item The diverse library of optimal conformations of the jointly optimal sequence
  for each relaxed PDB
  \marginnote{$\rightarrow$\texttt{.lib+} suffixes}.
\item the optimal PDB structure for the joint sequence on each relaxed
  backbone and associated energy decomposed by
  type\marginnote{$\rightarrow$\texttt{.opt+} and \texttt{.show+}
    suffix}.
\item the diverse library of optimal PDB structures for the joint sequences on each relaxed
  backbone and associated energy decomposed by
  type\marginnote{$\rightarrow$\texttt{.opt+lib} and \texttt{.show+lib}
    suffix}.
\item a dump of all pairwise energies and distances between residues
  in the optimal conformation for the joint
  sequence\marginnote{$\rightarrow$\texttt{.pwa+} suffix}.
\item an exhaustive list of sequences within a threshold of
  the optimal sequence\marginnote{$\rightarrow$\texttt{.enum+}
    suffix}. Each reported sequence is actually the repetition
  of the same sequence over all states.
\end{itemize}


\section{How can I control it ?}
\label{control}

The results you will get depend on a set of parameters\marginnote{In
  the \texttt{pars} directory}:
\begin{itemize}
\item \texttt{scorefunction}: contains the name of the energy
  force field to use\marginnote{Default is \texttt{beta\_nov15}}.
\item \texttt{rotamers}: density of the rotamer
  library.\marginnote{Default is Dunbrack's library with no
    extension.}  The \texttt{store} directory contains an
  \texttt{extended.rot} file that can be copied for denser
  rotamers. Can also be controlled by the \texttt{.resfile}
  of each \texttt{.pdb} file.
\item \texttt{nbrelax}: the number of relaxations applied to the
  initial PDB\marginnote{Default is 1}.
\item \texttt{nbfixbb}: the number of suboptimal conformations
  produced with \texttt{Rosetta} \texttt{fixbb} packing
  protocol\marginnote{Default is 1, usually enough}
\item \texttt{precision}: the number of digits after the decimal
  points used for representing energies during optimization. Ideally from 2 to
  8\marginnote{Default is 6}.
\item \texttt{threshold}: the threshold to the GMEC to use for
  enumeration of sequences in REU (Rosetta Energy Units).\marginnote{Default is $1.0$}
\item \texttt{nbenum}: the maximum number of enumerated
  sequences to produce. If you need all of them, put ``ALL''.\marginnote{Default is $10$}
\item \texttt{sampling}: the amount of conformations to sample per
  sequence during enumeration. Can be \texttt{ONE} or \texttt{ALL}
  (exhaustive counting)\marginnote{Default is \texttt{ONE}}.
\item \texttt{fixedseed}: controls how the Rosetta seed for
  pseudo-random number generation is initialized\marginnote{Default
    is fixed seed}. This is a booolean.  Set to False or delete the
  file to use Rosetta's default seed initialization process.
\item \texttt{verbose}: controls verbosity\marginnote{Default is
    1}. This is an integer from 0 to 5. Passed to Rosetta as
  \texttt{out:level} after multiplication by 100.
\item \texttt{resfile}: a Rosetta resfile used to initialize the
  required \texttt{.resfile} if absent\marginnote{Default is a side
    chain packing \texttt{resfile}}. See
  \href{https://www.rosettacommons.org/manuals/archive/rosetta3.5_user_guide/d1/d97/resfiles.html}{RosettaCommons}
  for details on resfiles.
\end{itemize}

You can also customize the extra information that is sent to PyMol
(energy and/or hbonds as detected by Rosetta) by changing the value of
the following parameters:
\begin{itemize}
\item \texttt{energy\_vis}: can be ``none'', ``all'' or the name of an
  energy term of the score function (eg. \texttt{fa\_rep}). The
  corresponding energy terms will be projected on the protein
  visualization on PyMol.
\item \texttt{hbond\_vis}: can be True or False. This allows to
  visuamize the hydrogen bonds as detected in Rosetta.
\item \texttt{hpatch}: can be 0 or 1. The hpatch option prevents creation of hydrophobic patches at the protein surface. If hpatch is set to 1 neighboring hydrophobic pairs at the protein surface are forbidden\marginnote{Default is 0}.
\end{itemize}

Additional information on each backbone can be provide by suitable
suffixed files. Each file should have the same basename as the PDB file
processed, using a specific extension as indicated below.

\begin{itemize}
\item \texttt{.dist}: If you want to add a distance constraint,
  describe it in a json-like \texttt{<PDBfile>.dist} file. It should
  contain as parameters the regions involved, the min and max number
  of mutations and the target sequence. Target can either be a
  sequence, or a FASTA filename containing the single target sequence
  (file extension must be \texttt{.fasta}). All positions must be
  described in the target sequence (as numbered in the pdb file).
  Several distance constraints can be given in
  \texttt{<PDBfile>.dist0}, \texttt{<PDBfile>.dist1}, ...
\item \texttt{.div}: If you want to generate a library of diverse optimal sequences,
  describe it in a json-like \texttt{<PDBfile>.div} file. It should
  contain as parameters the regions involved, the diversity threshold,
  and the number of sequences to be produced. Region positions should correspond
  to position numbering in the PDB file.
  
  In multi-state design, a single diversity file should be given.
\end{itemize}

\section{How can I use it ?}
\label{use_section}

\subsection{Single state design}

Copy one or more Rosetta-friendly PDB file, with suffix \texttt{.pdb}
in the directory with the
\texttt{Makefile}\marginnote{\texttt{1aho.pdb} and \texttt{0aho.pdb}
  are provided as examples with dummy \texttt{.resfile}s}. Have a look to
the parameters (in the \texttt{pars} directory. You can:

\begin{itemize}
\item produce the PDB file of the GMEC of a
  PDB\marginnote{\texttt{make 1aho.opt}}. A relaxed version of the PDB
  will be computed with its energy matrix, that will be
  optimised. Finally the optimal pose will be producted as a
  \texttt{.opt}.
\item enumerate all sequences within \texttt{threshold} of the GMEC of
  the PDB\marginnote{\texttt{make 1aho.enum}}.
\end{itemize}

Any of the previous suffixes can used as a target for
make\marginnote[-1em]{Try \texttt{make 1aho.pwa}}.

All \texttt{.pdb/.resfile} files in the
directory\marginnote{Try \texttt{make allopts}} can be processed in one
call\marginnote{or \texttt{make allenums}}. A simple
\texttt{make}, will do all GMEC and enumerations

\begin{itemize}
\item produce the PDB files of the library of diverse optimal conformations of a
  PDB\marginnote{\texttt{make 1aho.opt}}. A relaxed version of the PDB
  will be computed with its energy matrix, that will be
  optimised. Finally the optimal poses will be producted as a
  \texttt{.opt0}, \texttt{.opt1}, \texttt{.opt2}, etc.
\end{itemize}


\begin{itemize}
\item if you have several cores on your CPU, you can use the
  \texttt{-j} flag of make to process multiple backbones in
  parallel\marginnote{\texttt{make -j4}} (if enough RAM is available).
\item because it's based on make, you can edit the parameters you want
  and just type \texttt{make} to update all structures and sequences.
  Also, a \texttt{make showpars} will show you the current status of your
  parameters.
\end{itemize}

\subsection{Positive multistate design}

Similarly, you can produce, for each backbone, an optimal structure
for a jointly optimal sequence.\marginnote{\texttt{make 1aho.opt+}} as
well as a list of jointly suboptimal sequences\marginnote{\texttt{make
    1aho.enum+}}.

You can also produce, for each backbone, an optimal structure for a library of jointly optimal diverse sequences.\marginnote{\texttt{make 1aho.opt+}}

Because these computations require several independent intermediate
files to be produced, the use of parallelization (with \texttt{-j}) is
advised\marginnote{\texttt{make -j4 0aho.show+}}.

\subsection{How to generate models ensembles from X-ray structures using backrub}

In order to run POMPd on a X-ray protein structure, different
conformational states are needed as input files. To generate a
conformational ensemble of structures for a given protein, "backrub"
method in Rosetta was used.  This protocol outputs two sets of
structures; \textit{low} structures have the lowest energy
conformation for each simulation and \textit{last} structures have the
last conformation that was sampled in the simulation. In our protocol
100 structures were generated for each X-ray in the benchmark and
clustering procedure is used on each \textit{last} structure. The
following command line options were used:
\begin{verbatim}
mpirun -np 10 backrub.mpi.linuxgccrelease -in:file:s input.pdb -beta -nstruct 100 -backrub:ntrials 10000
\end{verbatim}
This step is followed by RMSD-based hierarchical clustering which is used to select (in our case) the four most diverse conformations among the 100. User defined clustering distance threshold (-d) is set to reach the desired number of clusters in the given list of pdb files previously generated by backrub.
Durandal software was used with following command line:
\begin{verbatim}
durandal -i pdb_list -d (integer) -o cluster -s -v -m 4 
\end{verbatim}

\subsection{How to modify/control weights of given states}

By default all states equally contribute to the total energy. If you
want to modify the weight of one state you need to create .weight file
containing the new weight as a real number.This file should have the
same basename as the PDB file processed.  \nobibliography{tb2cpd}
\bibliographystyle{plain}

\end{document}
